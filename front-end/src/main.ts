import Vue from 'vue'
import Buefy from 'buefy'
import App from './App.vue'
import router from './router'
import store from './store'
import VueI18n from 'vue-i18n'
import messages from '@/messages';
import FlagIcon from 'vue-flag-icon';
import axios, { AxiosRequestConfig } from 'axios';
import vueCookie from 'vue-cookie';
import { pick, get } from 'lodash';
import { AxiosError, AxiosResponse } from 'axios';
import { USER, AUTH } from '@/store';

import '@/assets/style.scss';

Vue.use(Buefy, {
  defaultIconPack: 'fas',
})

Vue.use(VueI18n);
Vue.use(FlagIcon);
Vue.use(vueCookie);

const i18n = new VueI18n({
  locale: 'fr',
  messages,
});

const axiosConfig: AxiosRequestConfig = {
  baseURL: process.env.VUE_APP_API_URI,
  timeout: 30000,
};

Vue.config.productionTip = false;
Vue.prototype.$http = axios.create(axiosConfig);

Vue.prototype.$http.interceptors.response.use(
  (response: AxiosResponse): AxiosResponse => {
  if (response.headers['access-token']) {
    const authHeaders = pick(response.headers,
                             ['access-token', 'client', 'expiry',
                              'uid', 'token-type']);
    store.commit(AUTH, authHeaders);

    let session = vueCookie.get('session');

    if (session) {
      session = JSON.parse(session);
      session['tokens'] = authHeaders;

      vueCookie.set('session', JSON.stringify(session),
                    { expires: '14D' });
    }
  }

  return response;
}, (error: AxiosError): Promise<any> => {
  if (router.currentRoute.name !== 'Auth' &&
      get(error, ['response', 'status']) === 401) {
    vueCookie.delete('session');
    store.commit(USER, null);
    store.commit(AUTH, {});
    router.push({ name: 'Auth' });
  }

  return Promise.reject(error);
})

Vue.prototype.$http.interceptors.request.use((config) => {
  const headers = store.getters['auth'];

  config.headers = headers;
  return config;
})

const existingSession = vueCookie.get('session');

if (existingSession && existingSession.length) {
  const session = JSON.parse(existingSession);
  store.commit(USER, session.user);
  store.commit(AUTH, session.tokens);
}

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')
