import Vue from 'vue'
import VueRouter, { Route, RouteConfig } from 'vue-router'
import store from '@/store'

import GamesIndex from '@/components/pages/games/GamesIndex.vue';
import GamesShow from '@/components/pages/games/GamesShow.vue';
import GamesRanking from '@/components/pages/games/GamesRanking.vue';
import Pantheon from '@/components/pages/Pantheon.vue';
import V1Ranking from '@/components/pages/V1Ranking.vue';
import FeathersRanking from '@/components/pages/FeathersRanking.vue';
import UserIndex from '@/components/pages/UserIndex.vue';
import Steps from '@/components/pages/Steps.vue';
import Auth from '@/components/pages/Auth.vue';

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    redirect: '/me',
  },
  {
    path: '/games',
    name: 'Games',
    component: GamesIndex,
  },
  {
    path: '/game/:id',
    name: 'Game',
    component: GamesShow,
  },
  {
    path: '/pantheon/:id',
    name: 'Pantheon',
    component: Pantheon,
  },
  {
    path: '/me',
    name: 'Me',
    component: UserIndex,
  },
  {
    path: '/user/:id',
    name: 'User',
    component: UserIndex,
  },
  {
    path: '/scores/steps',
    name: 'Steps',
    component: Steps,
  },
  {
    path: '/rankings/games/:id/:level',
    name: 'GamesRanking',
    component: GamesRanking,
  },
  {
    path: '/rankings/v1',
    name: 'V1Ranking',
    component: V1Ranking,
  },
  {
    path: '/rankings/feathers',
    name: 'FeathersRanking',
    component: FeathersRanking,
  },
  {
    path: '/auth',
    name: 'Auth',
    component: Auth,
    beforeEnter: (to: Route, from: Route, next) => {
      if (store.getters.isLogged) {
        next(false);
      } else {
        next();
      }
    },
  }
]

const router = new VueRouter({
  routes
})

router.beforeEach((to: Route, from: Route, next) => {
  if (to.name !== 'Auth' && !store.getters.isLogged) {
    next({ name: 'Auth' });
  } else {
    next();
  }
})

export default router
