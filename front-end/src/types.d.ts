import Vue from "vue";

declare module "vue/types/vue" {
  interface Vue {
    $cookie: any;
    $http: any;
  }
}
