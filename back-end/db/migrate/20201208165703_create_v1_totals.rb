class CreateV1Totals < ActiveRecord::Migration[6.0]
  def change
    create_table :v1_totals do |t|
      t.integer :user_id
      t.integer :period_id
      t.integer :value

      t.timestamps
    end
  end
end
