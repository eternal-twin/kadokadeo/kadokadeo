# frozen_string_literal: true

class User < ActiveRecord::Base
  extend Devise::Models
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  include DeviseTokenAuth::Concerns::User

  has_many :favourites, dependent: :destroy
  has_many :scores
  has_many :v1_totals
  has_many :levels

  after_create :init_game_levels

  def feathers_count
    @feathers ||= self.levels.paradise
                             .map(&:game)
                             .reduce(0) do |acc, game|
      # Check Score#period_rank
      score = self.scores.period.find_by(game: game)
      acc += 1 if score&.period_rank == 1
      acc
    end
  end

  def compute_v1_total
    best_v1_scores = self.scores.period
                         .select('MAX(v1_score)')
                         .order(max: :desc)
                         .limit(12)
                         .group(:game_id)
    total = Score.unscoped
                 .select('SUM(max) as v1_total')
                 .from(best_v1_scores)

    total[0].v1_total
  end

  def v1_total
    self.v1_totals
        .find_by(period: Period.last)
        &.value.to_i
  end

  def v1_period_rank
    v1_totals = V1Total.period.select(
                  'row_number() OVER (ORDER BY value DESC) as rank',
                  :user_id
                )
    V1Total.unscoped
           .select(:rank)
           .from(v1_totals)
           .where('user_id = ?', self.id)
           .limit(1)[0]&.rank
  end

  def trophies
    self.levels.group(:name).count
  end

  def level_for_game(game)
    self.levels.find_by(game: game)
  end

  def as_json(options={})
    super(
      only: [:id, :email],
      methods: options[:methods]
    )
  end

  private

  def init_game_levels
    Game.all.each do |game|
      self.levels.create(
        name: Level.names.keys.first,
        game: game
      )
    end
  end
end
