class V1Total < ApplicationRecord
  belongs_to :user
  belongs_to :period

  scope :period, -> { where(period: Period.last) }
end
