class Game < ApplicationRecord
  include StepsConcern
  include LevelsConcern
  enum mode: [:action, :puzzle]
  has_many :scores

  def self.with_favourites(user)
    self.select(:id, :name, :mode,
                "favourites.id as pinned")
        .joins("LEFT JOIN favourites
                ON games.id = favourites.game_id
                AND favourites.user_id = #{user.id}")
  end

  def steps
    self.class.game_steps[self.name]
  end
end
