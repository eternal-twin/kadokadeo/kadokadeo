class Level < ApplicationRecord
  enum name: [:newbie, :confirmed, :green, :orange,
              :red, :bronze, :silver, :gold, :paradise]
  belongs_to :user
  belongs_to :game

  Level.names.keys.each do |name|
    scope name.to_sym, -> { where(name: name) }
  end

  def increase!
    return if self.name == 'paradise'

    current_level_key = read_attribute_before_type_cast(:name)
    next_level_name = self.class
                          .names.keys[current_level_key + 1]
    self.update(name: next_level_name)
  end
end
