require 'active_support/concern'

module LevelsConcern
  extend ActiveSupport::Concern

  def level
    # Use for json rendering
    return unless self.level_int
    Level.names.keys[self.level_int]
  end
end
