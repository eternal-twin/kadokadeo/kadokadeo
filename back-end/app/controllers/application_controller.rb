class ApplicationController < ActionController::API
  include DeviseTokenAuth::Concerns::SetUserByToken

  rescue_from ActiveRecord::RecordNotFound, with: :render_404

  def render_404
    render status: :not_found
  end

  def ressource_exists(ressource, id)
    ressource.find(id)
  end
end
