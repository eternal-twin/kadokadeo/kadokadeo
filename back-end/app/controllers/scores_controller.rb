class ScoresController < ApplicationController
  before_action :authenticate_user!
  before_action :set_game, except: [:feathers_ranking,
                                    :v1_ranking, :steps]

  def add
    score = params[:score]

    current_user.scores
                .create(game: @game,
                        period: Period.last,
                        value: score)
    render nothing: true, status: :created
  end

  def pantheon
    scores = Score.best_by_user(game: @game)
    render json: { game: @game, scores: scores }
  end

  def game_ranking
    level_int = Level.names[params[:level_name]]
    unless level_int.in?(Level.names.values)
      return render status: :bad_request
    end
    scores = Score.game_ranking_by_level(game: @game,
                                         level_int: level_int)
    render json: { game: @game, scores: scores }
  end

  def feathers_ranking
    users_with_paradise = User.joins(:levels)
                              .where(levels: { name: :paradise })
                              .distinct
    users_with_paradise = users_with_paradise.select do |user|
      user.feathers_count.positive?
    end.sort_by do |user|
      -user.feathers_count
    end
    render json: users_with_paradise
                  .as_json(methods: :feathers_count)
  end

  def v1_ranking
    scores = V1Total.select(:user_id, :value, :email,
                            :created_at)
                    .period
                    .order(value: :desc)
                    .joins(:user)
    render json: { scores: scores }
  end

  def steps
    render json: Score.game_steps
  end

  private

  def set_game
    @game = Game.find(params[:game_id])
  end
end
