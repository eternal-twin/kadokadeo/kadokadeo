class PeriodsController < ApplicationController
  def current
    period = Period.last
    day = ((Time.zone.now.beginning_of_day.to_i -
           period.start_date.beginning_of_day.to_i) / 86400) + 1
    render json: { day: day, id: period.id }
  end
end
