class UsersController < ApplicationController
  before_action :authenticate_user!

  def index
    user = User.find_by(id: params[:id]) || current_user
    scores = user.scores.period
                 .select('MAX(value) as value', 'games.name',
                         'MAX(star) as star',
                         'MAX(levels.name) as level_int',
                         'MAX(v1_score) as v1_score',
                         :game_id, :user_id) # For period_rank
                 .left_joins(:game)
                 .joins('LEFT JOIN levels
                         ON games.id = levels.game_id
                         AND levels.user_id = scores.user_id')
                 .group(:game_id, 'games.name', :user_id)
                 .order('v1_score DESC')

    render json: {
      user: user.as_json(methods: [:v1_total, :v1_period_rank,
                                   :trophies, :feathers_count]),
      scores: scores.as_json(methods: [:period_rank, :level],
                             except: [:id, :user_id, :level_int])
    }
  end

  def search
    match = params[:match]
    users = User.select(:id, :email)
                .where('email LIKE ?', "%#{match}%")

    render json: users
  end

  def highscores
    user = (id = params[:id]) ? User.find(id) : current_user
    # Select on Score so rails translate star enum
    highscores = Score.select('games.name', 'games.id',
                              'COALESCE(MAX(value), 0) as value',
                              'MAX(star) as star',
                              'COALESCE(MAX(levels.name), 0) as level_int')
                      .joins("RIGHT JOIN games
                              ON games.id = scores.game_id
                              AND scores.user_id = #{user.id}")
                      .joins("LEFT JOIN levels
                              ON scores.game_id = levels.game_id
                              AND levels.user_id = #{user.id}")
                      .group('games.id')
                      .order('games.name')
    render json: highscores.as_json(methods: :level,
                                    except: :level_int)
  end
end
