class GamesController < ApplicationController
  before_action :authenticate_user!

  def index
    games = Game.with_favourites(current_user)
    render json: games
  end

  def show
    game_id = params[:id]
    ressource_exists(Game, game_id)
    user_id = current_user.id
    game = Game.select('games.id', :name, :mode,
                       'favourites.id as pinned',
                       'COALESCE(MAX(user_scores.value), 0) as "userBest"',
                       'COALESCE(MAX(user_period_scores.value), 0) as "userPeriodBest"',
                       'COALESCE(MAX(scores.value), 0) as best',
                       'COALESCE(MAX(period_scores.value), 0) as feather',
                       'MAX(levels.name) as level_int')
               .joins("LEFT JOIN scores as user_scores
                       ON games.id = user_scores.game_id
                       AND user_scores.user_id = #{user_id}")
               .joins("LEFT JOIN scores as user_period_scores
                       ON games.id = user_period_scores.game_id
                       AND user_period_scores.period_id = #{Period.last.id}
                       AND user_period_scores.user_id = #{user_id}")
               .joins("LEFT JOIN scores as period_scores
                       ON games.id = period_scores.game_id
                       AND period_scores.period_id = #{Period.last.id}")
               .left_joins(:scores)
               .joins("LEFT JOIN favourites
                       ON games.id = favourites.game_id
                       AND favourites.user_id = #{user_id}")
               .joins("LEFT JOIN levels
                       ON games.id = levels.game_id
                       AND levels.user_id = #{user_id}")
               .where('games.id = ?', game_id)
               .group('games.id, favourites.id')
               .limit(1)
               .first

    render json: game.as_json(methods: [:steps, :level])
  end

  def all
    render json: Game.all.as_json(only: [:name, :id])
  end
end
