Rails.application.routes.draw do
  mount_devise_token_auth_for 'User', at: 'api/auth'

  scope '/api' do
    get '/periods/current', to: 'periods#current'

    post 'favourites/toggle', to: 'favourites#toggle'

    scope '/games' do
      get '/index', to: 'games#index'
      get '/all', to: 'games#all'
      get '/show/:id', to: 'games#show'
    end

    scope '/scores' do
      post '/add', to: 'scores#add'
      get '/pantheon/:game_id', to: 'scores#pantheon'
      get '/ranking/:game_id/:level_name', to: 'scores#game_ranking'
      get '/steps', to: 'scores#steps'
      get '/v1_ranking', to: 'scores#v1_ranking'
      get '/feathers_ranking', to: 'scores#feathers_ranking'
    end

    scope 'users' do
      get '/show/:id', to: 'users#index'
      get '/search/:match', to: 'users#search'
      get '/highscores(/:id)', to: 'users#highscores'
    end
  end
end
